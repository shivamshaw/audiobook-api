<?php
include('include/db.php');

switch ($_GET['action']) {

    case 'createaudio':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            if (token($data['uid']) == $data['token']) {
                unset($data['token']);
                $aid = insert('INSERT INTO `audio`(`a_uid`, `a_title`, `a_rating`, `a_lcount`, `a_ccount`, `a_ptime`, `a_genre`, `a_status`) VALUES (:uid, :title, :rating, :lcount, :ccount, :ptime, :genre, :status)', $data);
                if (isset($aid)) echo json_encode(array(
                    'msg' => 'Audio created successfully',
                    'status' => 'success',
                    'data' => array(
                        'aid' => $aid
                    )
                ));
            } else {
                response('tokenmismatch');
            }
        }
        break;

    case 'uploadassets':
        //print_r($_POST);
        //print_r($_FILES);
        header('Content-type: application/json');
        if (!empty($_POST) && !empty($_FILES)) {
            if (token($_POST['uid']) == $_POST['token']) {
                if (isset($_FILES['image'])) {
                    $tmpName  = $_FILES['image']['tmp_name'];
                    $imageFileType = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
                    $uploadpath = "upload/" . $_POST['aid'] . '.' . $imageFileType;
                    if ($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg") {
                        if (copy($tmpName, $uploadpath)) {
                            $output['image'] = array(
                                'msg' => 'File uploaded',
                                'status' => 'success',
                                'data' => array(
                                    'uid' => $_POST['uid'],
                                    'uploadpath' => $uploadpath
                                )
                            );
                        }
                    } else {
                        $output['image'] = array(
                            'msg' => 'Sorry, only JPG, JPEG, PNG  files are allowed.',
                            'status' => 'error'
                        );
                    }
                }
                if (isset($_FILES['audio'])) {
                    $tmpName  = $_FILES['audio']['tmp_name'];
                    $imageFileType = strtolower(pathinfo($_FILES['audio']['name'], PATHINFO_EXTENSION));
                    $uploadpath = "upload/" . $_POST['aid'] . '.' . $imageFileType;
                    if ($imageFileType == "mp3") {
                        if (copy($tmpName, $uploadpath)) {
                            $output['audio'] = array(
                                'msg' => 'File uploaded',
                                'status' => 'success',
                                'data' => array(
                                    'uid' => $_POST['uid'],
                                    'uploadpath' => $uploadpath
                                )
                            );
                        }
                    } else {
                        $output['audio'] = array(
                            'msg' => 'Sorry, only MP3 files are allowed.',
                            'status' => 'error'
                        );
                    }
                }
                echo json_encode($output);
            } else {
                response('tokenmismatch');
            }
        }


        break;

    case 'myaudio':
        $data = json_decode(file_get_contents('php://input'), 1);
        //print_r($data);

        if (!empty($data)) {
            $audio = fetchAll('SELECT * FROM `audio` WHERE `a_uid` = :uid', array('uid' => $data['uid']));
            if (token($data['uid']) == $data['token'])
                response('myaudio', array('data' => $audio, 'pfx' => 'a'));
            else {
                response('tokenmismatch');
            }
        }


        break;

    case 'allaudio':
        $data = json_decode(file_get_contents('php://input'), 1);
        //print_r($data);

        if (!empty($data)) {
            if (token($data['uid']) == $data['token']) {
                switch ($data['sort']) {

                    case 'time': {
                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_ptime` LIMIT 10';
                            else $orderby = ' ORDER BY `a_ptime` DESC LIMIT 10';
                        }
                        break;

                    case 'rating': {

                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_rating` LIMIT 10';
                            else $orderby = ' ORDER BY `a_rating` DESC LIMIT 10';
                        }
                        break;

                    case 'like': {

                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_lcount` LIMIT 10';
                            else $orderby = ' ORDER BY `a_lcount` DESC LIMIT 10';
                        }
                        break;

                    case 'duration': {

                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_duration` LIMIT 10';
                            else $orderby = ' ORDER BY `a_duration` DESC LIMIT 10';
                        }
                        break;

                    case 'popular': {

                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_ccount` LIMIT 10';
                            else $orderby = ' ORDER BY `a_ccount` DESC LIMIT 10';
                        }
                        break;

                    case 'listens': {

                            if ($data['mode'] == 'asc') $orderby = ' ORDER BY `a_listens` LIMIT 10';
                            else $orderby = ' ORDER BY `a_listens` DESC LIMIT 10';
                        }
                        break;
                }

                $where = '';
                foreach ($data['filters'] as $filter => $value) {

                    switch ($filter) {

                        case 'production':
                            $where .= ' AND `a_pid` = :pid';
                            $params['pid'] = $value;
                            break;

                        case 'artist':
                            $where .= ' AND `a_arids` = :aid';
                            $params['aid'] = $value;
                            break;

                        case 'genre':
                            $where .= ' AND `a_genre` = :genre';
                            // $value = implode("', '",explode(',',$value));
                            // $value = stripslashes(trim($value)); 
                            $params['genre'] = $value;
                            break;
                    }
                }

                $sql = 'SELECT * FROM `audio` WHERE `a_status` = "Publish"';

                $sql = $sql . $where . $orderby;

                $audio = fetchAll($sql, $params, 1);

                response('myaudio', array('data' => $audio, 'pfx' => 'a'));
            } else {
                response('tokenmismatch');
            }
        }


        break;

    case 'comment':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            if (token($data['uid']) == $data['token']) {
                $comment = insert('INSERT INTO `comment`( `c_aid`, `c_uid`, `c_comment`,`c_time`) VALUES ( :aid, :uid, :comment, :time)', array(
                    'aid' => $data['aid'],
                    'uid' => $data['uid'],
                    'comment' => $data['comment'],
                    'time' => time()
                ));
                if (isset($comment)) {
                    echo json_encode(array(
                        'status' => 'success',
                        'msg' => 'Successful'
                    ));
                } else
                    echo json_encode(array(
                        'status' => 'error',
                        'msg' => 'Unsuccessful'
                    ));
            } else
                response('tokenmismatch');
        }

        break;

    case 'like':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            if (token($data['uid']) == $data['token']) {
                $affected = rowCount('UPDATE `audio` SET `a_lcount`= a_lcount+1 WHERE a_id = :aid', array('aid' => $data['aid']));
                if (isset($affected)) {
                    echo json_encode(array(
                        'status' => 'success',
                        'msg' => 'liked'
                    ));
                } else
                    echo json_encode(array(
                        'status' => 'error',
                        'msg' => 'Please try again'
                    ));
            } else {
                response('tokenmismatch');
            }
        }

        break;
}
