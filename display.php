<?php
include_once('include/db.php');
$students = fetchAll('SELECT * FROM `registration`', $_GET);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="2">
        <tr>
            <td>link</td>
            <td>fname</td>
            <td>lname</td>
            <td>course</td>
            <td>gender</td>
            <td>mobile</td>
            <td>address</td>
            <td>email</td>
            <td>password</td>
            <td>image</td>
            <td>DOB</td>
        </tr>
        <?php
        //print_r($students);
        foreach ($students as $student) { ?>
            <tr>
                <td>
                <a href="edit.php?id=<?php echo $student['s_id'] ?>">Edit</a>
                <a href="delete.php?id=<?php echo $student['s_id'] ?>" onclick="return confirm('Are your sure to delete?')">Delete</a>
                </td>
                <td><?php echo $student['s_fname'] ?></td>
                <td><?php echo $student['s_lname'] ?></td>
                <td><?php echo $student['s_course'] ?></td>
                <td><?php echo $student['s_gender'] ?></td>
                <td><?php echo $student['s_mobile'] ?></td>
                <td><?php echo $student['s_address'] ?></td>
                <td><?php echo $student['s_email'] ?></td>
                <td><?php echo $student['s_password'] ?></td>
                <td><?php echo $student['s_image'] ?></td>
                <td><?php echo date('d/m/y H:i', $student['s_dob']) ?></td>
            </tr>
        <?php } ?>
    </table>
</body>

</html>