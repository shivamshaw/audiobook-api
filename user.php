<?php
include('include/db.php');

switch ($_GET['action']) {

    case 'login':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            $user = fetch('SELECT * FROM `user` WHERE `u_name` = :username  AND `u_password` = md5(:password)', $data);
            $token = md5($user['u_id'] . $user['u_password'] . SKEY);
            unset($user['u_password']);
            response('login', array('data' => $user, 'pfx' => 'u', 'token' => $token));
        }

        break;

    case 'registration':
        $data = json_decode(file_get_contents('php://input'), 1);
        $data['regtime'] = strtotime(date('d-m-y h:i:s'));
        $data['acctime'] = strtotime(date('d-m-y h:i:s'));

        if (!empty($data)) {

            $id = insert('INSERT INTO `user` (`u_id`, `u_name`, `u_email`, `u_gender`, `u_mobile`, `u_password`, `u_membership`, `u_status`, `u_regtime`, `u_acctime`) VALUES (NULL, :name, :email, :gender, :mobile, md5(:password), :membership, :status, :regtime, :acctime)', $data);
            response('insert', array('id' => $id));
        }

        break;

    case 'updatepassword':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            if (token($data['uid']) == $data['token']) {
                $user = fetch('SELECT * FROM `user` WHERE `u_id` = :uid  AND `u_password` = md5(:password)', array('uid' => $data['uid'], 'password' => $data['old_password']));
                if (count($user)) {
                    $affected = rowCount('UPDATE `user` SET `u_password`= MD5(:password) WHERE u_id = :uid', array('uid' => $data['uid'], 'password' => $data['new_password']));
                    if (isset($affected) && $affected == 1) {
                        response('passwordupdated');
                    } else {
                        echo json_encode(array(
                            'status' => 'error',
                            'msg' => 'Try again later'
                        ));
                    }
                } else {
                    echo json_encode(array(
                        'status' => 'error',
                        'msg' => "Old Password dosn't match'"
                    ));
                }
            } else {
                response('tokenmismatch');
            }
        }

        break;

    case 'sendotp':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            $user = fetch('SELECT * FROM `user` WHERE `u_status` = "Active" AND `u_name` = :username', $data);
            if (isset($user)) {
                $affected = rowCount('UPDATE `user` SET `u_otp`= :otp WHERE `u_id` = :uid', array('otp' => rand(100000, 999999), 'uid' => $user['u_id']));
                if (isset($affected)) {
                    echo json_encode(array(
                        'status' => 'otp sent',
                        'msg' => 'check your mail'
                    ));
                }
            }
        }

        break;

    case 'setpassword':
        $data = json_decode(file_get_contents('php://input'), 1);

        if (!empty($data)) {
            $user = fetch('SELECT * FROM `user` WHERE `u_name` = :username AND `u_otp` = :otp', array('username' => $data['username'], 'otp' => $data['otp']));
            //print_r($user);
            if (isset($user['u_id'])) {
                $affected = rowCount('UPDATE `user` SET `u_password`= MD5(:password) WHERE u_name = :username', array('username' => $data['username'], 'password' => $data['new_password']));
                if (isset($affected) && $affected == 1) {
                    echo json_encode(array(
                        'status' => 'password updated',
                        'msg' => 'Successful'
                    ));
                } else echo json_encode(array(
                    'status' => 'error',
                    'msg' => 'please try again'
                ));
            } else echo json_encode(array(
                'status' => 'error',
                'msg' => 'please enter correct otp'
            ));
        }

        break;
}
