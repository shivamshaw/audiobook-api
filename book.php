<?php
//include('include/db1.php');
include('include/db.php');

foreach (getallheaders() as $i => $v) $newheaders[strtolower($i)] = $v;
extract($newheaders, EXTR_PREFIX_ALL, 'hd');
if (isset($hd_token) && $hd_token == 'fhgiughikdfsngikuwhnti45l43j5834984ref9yhf943q') {

    switch ($_SERVER['REQUEST_METHOD']) {

        case 'GET':

            if (!empty($_GET['id'])) {
                $retrieve = fetch('SELECT * FROM `registration` WHERE `s_id` = :id', $_GET);
                response('fetch', array('data' => $retrieve, 'pfx' => 's'));
            } else {
                $retrieveall = fetchall('SELECT * FROM `registration`');
                response('fetchall', array('data' => $retrieveall, 'pfx' => 's'));
            }

            break;


        case 'POST':
            $data = json_decode(file_get_contents('php://input'), 1);
            if (!empty($data)) {
                $id = insert('INSERT INTO `registration` VALUES (NULL, :fname, :lname, :course, :gender, :mobile, :address, :email, MD5(:password), :image, :dob)', $data);
                response('insert', array('id' => $id));
            }
            break;


        case 'PATCH':

            $input = json_decode(file_get_contents('php://input'), 1);
            if (!empty($input)) {
                $sql = 'UPDATE `registration` SET ';
                foreach ($input as $i => $v) {
                    if ($i != 'id') {
                        if ($i == 'password') $sql .= "`s_$i`= MD5(:$i), ";
                        else $sql .= "`s_$i`= :$i, ";
                    }
                }

                $sql = substr($sql, 0, -2) . ' WHERE `s_id` = :id';
                $affected = rowCount($sql, $input);
                response('update', array('affected' => $affected));
            }
            break;


        case 'DELETE':

            if (!empty($_GET['id'])) {
                $affected = rowCount('DELETE FROM `registration` WHERE `s_id` IN (:id)', $_GET);
                response('delete', array('affected' => $affected));
            }
            break;
    }
} else {
    header("HTTP/1.0 401 Authentication bad/failed");
    response('unauthorised');
}
