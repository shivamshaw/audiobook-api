-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2020 at 04:05 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `audiobook`
--

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `ar_id` int(11) NOT NULL,
  `ar_name` text COLLATE utf8_unicode_ci NOT NULL,
  `ar_image` text COLLATE utf8_unicode_ci NOT NULL,
  `ar_status` enum('Active','Inactive','Suspended') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `a_id` int(11) NOT NULL,
  `a_uid` int(11) NOT NULL,
  `a_title` text COLLATE utf8_unicode_ci NOT NULL,
  `a_genre` enum('Horror','Comedy','Thriller') COLLATE utf8_unicode_ci NOT NULL,
  `a_image` text COLLATE utf8_unicode_ci NOT NULL,
  `a_covimage` text COLLATE utf8_unicode_ci NOT NULL,
  `a_rating` float NOT NULL,
  `a_lcount` int(11) NOT NULL,
  `a_ccount` int(11) NOT NULL,
  `a_ptime` int(11) NOT NULL,
  `a_status` enum('Published','Unpublished','Moderator') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Moderator'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`a_id`, `a_uid`, `a_title`, `a_genre`, `a_image`, `a_covimage`, `a_rating`, `a_lcount`, `a_ccount`, `a_ptime`, `a_status`) VALUES
(1, 1, 'b jhn hjnhjn jhn hn n', 'Horror', 'gsgsgsgsg/gsgsg', 'gsgsgsgsgd/gsgsgd', 4.2, 12, 20, 1604727119, 'Published'),
(2, 2, 'dsjkdfsdkfkdfksfksfksdfjsfjks', 'Comedy', 'gsgsgsgsg/gsgsg', 'gsgsgsgsgd/gsgsgd', 4.1, 5, 21, 1604724178, 'Moderator'),
(3, 3, 'dsjkdfsdkfkdfksfksfksdfjsfjksss', 'Thriller', 'gsgsgsgsg/gsgsg', 'gsgsgsgsgd/gsgsgd', 4, 12, 2, 1604726231, 'Unpublished');

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `p_image` text COLLATE utf8_unicode_ci NOT NULL,
  `p_status` enum('Active','Inactive','Inactive') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(11) NOT NULL,
  `u_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `u_email` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `u_gender` enum('Male','Female','Others') COLLATE utf8_unicode_ci NOT NULL,
  `u_mobile` int(13) NOT NULL,
  `u_password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `u_membership` enum('Platinum','Gold','Silver') COLLATE utf8_unicode_ci NOT NULL,
  `u_status` enum('Active','Inactive','Suspended') COLLATE utf8_unicode_ci NOT NULL,
  `u_regtime` int(10) NOT NULL,
  `u_acctime` int(10) NOT NULL,
  `u_otp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_name`, `u_email`, `u_gender`, `u_mobile`, `u_password`, `u_membership`, `u_status`, `u_regtime`, `u_acctime`, `u_otp`) VALUES
(1, 'chayanhaldar', 'chayanhaldar67@gmail.com', 'Male', 2147483647, '827ccb0eea8a706c4c34a16891f84e7b', 'Platinum', 'Active', 1353387062, 1353387062, 385936),
(2, 'sankhaweb', 'sankhaweb@gmail.com', 'Male', 2147483647, '827ccb0eea8a706c4c34a16891f84e7b', 'Platinum', 'Active', 1384941468, 1384941468, 970193),
(3, 'shivamshaw', 'shivamshawck@gmail.com', 'Male', 2147483647, '827ccb0eea8a706c4c34a16891f84e7b', 'Platinum', 'Active', 1384941511, 1384941511, 107920);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`ar_id`);

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`,`u_mobile`,`u_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `ar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
