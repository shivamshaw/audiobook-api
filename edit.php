<?php
include_once('include/db.php');
if(!empty($_POST)){
    $sql = 'UPDATE `registration` SET ';
    foreach ($_POST as $i => $v) {
        if ($i != 'id') {
            if ($i == 'password') $sql .= "`s_$i`= MD5(:$i), ";
            else $sql .= "`s_$i`= :$i, ";
        }
    }
    $sql = substr($sql, 0, -2) . ' WHERE `s_id` = :id';
    $_POST['dob'] = strtotime($_POST['dob']);
    //print_r($_POST);die();

    $affected = rowCount($sql, $_POST);
    header('location:  display.php');
}
$student = fetch('SELECT * FROM `registration` WHERE `s_id` = :id', $_GET);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <input type="hidden" name="id" value="<?php echo $student['s_id'] ?>">
        <input type="text" placeholder="fname" name="fname" value="<?php echo $student['s_fname'] ?>"><br><br>
        <input type="text" placeholder="lname" name="lname" value="<?php echo $student['s_lname'] ?>"><br><br>
        <input type="text" placeholder="course" name="course" value="<?php echo $student['s_course'] ?>"><br><br>
        <input type="text" placeholder="gender" name="gender" value="<?php echo $student['s_gender'] ?>"><br><br>
        <input type="text" placeholder="mobile" name="mobile" value="<?php echo $student['s_mobile'] ?>"><br><br>
        <input type="text" placeholder="address" name="address" value="<?php echo $student['s_address'] ?>"><br><br>
        <input type="text" placeholder="email" name="email" value="<?php echo $student['s_email'] ?>"><br><br>
        <input type="password" placeholder="password" name="password" value="<?php echo $student['s_password'] ?>"><br><br>
        <input type="text" placeholder="image" name="image" value="<?php echo $student['s_image'] ?>"><br><br>
        <input type="text" placeholder="dob" name="dob" value="<?php echo date('d-m-Y', $student['s_dob']) ?>"><br><br>
        <button type="submit">Update</button>
    </form>

</body>
</html>
<!-- SELECT `s_id`, `s_fname`, `s_lname`, `s_course`, `s_gender`, `s_mobile`, `s_address`, `s_email`, `s_password`, `s_image`, `s_dob` FROM `registration` WHERE 1 -->