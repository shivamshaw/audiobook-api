<?php

if ($_SERVER['HTTP_HOST'] == 'localhost') {
    define('SERVER', 'localhost');
    define('DATABASE', 'audiobook');
    define('USER', 'root');
    define('PASSWORD', '');
    define('SKEY', 'php');
}
else {
    define('SERVER', 'localhost');
    define('DATABASE', 'webstzsu_hosting_audiobook');
    define('USER', 'webstzsu_hosting_audiobook');
    define('PASSWORD', 'B,lK)}K)^iXb');
}

function getDB(): PDO
{
    try {
        $dbConnection = new PDO('mysql:host=' . SERVER . ';dbname=' . DATABASE, USER, PASSWORD);
        $dbConnection->exec("set names utf8");
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbConnection;
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function insert(string $sql, array $params = [], $debug = false)
{
    try {
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->execute($params);
        if ($debug) $stmt->debugDumpParams();
        $inserteid = (int)$db->lastInsertId();
        unset($db);
        return $inserteid; //Return inserted id
    } catch (PDOException $e) {
        return $e;
    }
}

function execute(string $sql, $params = array(), $debug = false)
{
    try {
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->execute($params);
        if ($debug) $stmt->debugDumpParams();
        unset($db);
        return $stmt;
    } catch (PDOException $e) {
        return $e;
    }
}

function fetchAll(string $sql, array $params = [], $debug = false): array
{
    $stmt = execute($sql, $params, $debug);
    if ($stmt->rowCount() > 0)
        return $stmt->fetchAll(PDO::FETCH_ASSOC); //Return array of multiple rows
    else return [];
}

function fetch(string $sql, array $params = [], $debug = false): array
{
    $stmt = execute($sql, $params, $debug);
    if ($stmt->rowCount())
        return $stmt->fetch(PDO::FETCH_ASSOC); //Return single row
    else return [];
}

function rowCount(string $sql, array $params = [], $debug = false): int
{
    $stmt = execute($sql, $params, $debug);
    return $stmt->rowCount();   //Return affectd rows
}

function token($uid)
{
    $user = fetch('SELECT * FROM `user` WHERE `u_status` = "Active" AND `u_id` = :uid', array('uid' => $uid));
    if(isset($user['u_id'])) return md5($user['u_id'] . $user['u_password'] . SKEY);
    else return NULL;
}

function response($action = '', $extra = [])
{

    if (!empty($action)) {

        switch ($action) {

            case 'fetch': {

                    if (count($extra['data'])) $response = array(
                        'msg' => '1 record found',
                        'status' => 'success',
                        'data' => $extra['data']
                    );
                    else $response = array(
                        'msg' => 'No record(s) found',
                        'status' => 'error'
                    );
                }
                break;

            case 'fetchall': {
                    if (count($extra['data'])) $response = array(
                        'msg' => count($extra['data']) . ' record(s) found',
                        'status' => 'success',
                        'data' => $extra['data']
                    );
                    else $response = array(
                        'msg' => 'No record(s) found',
                        'status' => 'error'
                    );
                }
                break;

            case 'insert': {
                    if (!empty($extra['id'])) {
                        if (is_int($extra['id']))
                            $response = array(
                                'msg' => 'Record inseted with ID : ' . $extra['id'],
                                'status' => 'success',
                                'insertId' => $extra['id']
                            );
                        else {
                            try {
                                list($value, $column) = explode(' for key ', $extra['id']->errorInfo[2]);
                                list(, $value) = explode("'", $value);
                                $column = substr($column, 1, -1);
                                switch ($column) {
                                    case 'u_mobile':
                                        $msg = "$value is already registered with us, Please try with another mobile number";
                                        break;

                                    case 'u_email':
                                        $msg = "$value is already registered with us, Please try with another email id";
                                        break;
                                }
                            } catch (Exception $e) {
                                $msg = $extra['id']->errorInfo[2];
                            }

                            $response = array(
                                'msg' => $msg,
                                'status' => 'error',
                            );
                        }
                    } else $response = array(
                        'msg' => 'No record inseted',
                        'status' => 'error'
                    );
                }
                break;

            case 'delete': {
                    if (!empty($extra['affected']))
                        $response = array(
                            'msg' => $extra['affected'] . ' records deleted',
                            'status' => 'success',
                            'affcted' => $extra['affected']
                        );
                    else $response = array(
                        'msg' => 'No record(s) deleted',
                        'status' => 'error'
                    );
                }
                break;

            case 'update': {
                    if (!empty($extra['affected'])) {
                        if (is_int($extra['affected']))
                            $response = array(
                                'msg' => $extra['affected'] . ' record(s) updated',
                                'status' => 'success',
                                'affcted' => $extra['affected']
                            );
                        else $response = array(
                            'msg' => $extra['affected']->errorInfo[2],
                            'status' => 'error'
                        );
                    } else $response = array(
                        'msg' => 'No records updated',
                        'status' => 'error'
                    );
                }
                break;

            case 'login': {
                    if (count($extra['data'])) $response = array(
                        'msg' => '1 record found',
                        'status' => 'success',
                        'data' => $extra['data'],
                        'token' => $extra['token']
                    );
                    else $response = array(
                        'msg' => 'No record(s) found',
                        'status' => 'error'
                    );
                }
                break;

            case 'myaudio': {
                    if (count($extra['data'])) $response = array(
                        'msg' => count($extra['data']) . ' record(s) found',
                        'status' => 'success',
                        'data' => $extra['data']
                    );
                    else $response = array(
                        'msg' => 'No record(s) found',
                        'status' => 'error'
                    );
                }
                break;

            case 'passwordupdated': {

                    $response = array(
                        'msg' => 'Password Updated Successful',
                        'status' => 'Successful'
                    );
                }
                break;

            case 'unauthorised':
                $response = array(
                    'msg' => 'Unauthorized access',
                    'status' => 'error'
                );
                break;
            case 'tokenmismatch':
                $response = array(
                    'msg' => 'Token Mismatched',
                    'status' => 'error'
                );
                break;
        }
        $json = json_encode($response);
        if (isset($extra['pfx'])) echo str_replace(
            array('{"' . $extra['pfx'] . '_', ',"' . $extra['pfx'] . '_'),
            array('{"', ',"'),
            $json
        );
        else echo $json;
    } else {
        echo json_encode(
            array(
                'msg' => 'No action needed',
                'status' => 'error'
            )
        );
    }
}
